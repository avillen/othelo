package es.uam.eps.dadm.server;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.dadm.database.RoundRepository;
import es.uam.eps.dadm.othello.Round;

public class ServerRepository implements RoundRepository {
    private static final String DEBUG = "ServerRepository";
    private static ServerRepository repository;
    private final Context context;
    private ServerInterface is;

    public static ServerRepository getInstance(Context context) {
        if (repository == null)
            repository = new ServerRepository(context.getApplicationContext());
        return repository;
    }

    private ServerRepository(Context context) {
        this.context = context.getApplicationContext();
        is = ServerInterface.getServer(context);
    }

    @Override
    public void open() throws Exception {
    }

    @Override
    public void close() {
    }

    public void loginOrRegister(final String playerName, String password, boolean register,
                                final RoundRepository.LoginRegisterCallback callback) {
        is.login(playerName, password, null, register,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        String uuid = result.trim();
                        if (uuid.equals("-1") || uuid.length() < 10) {
                            callback.onError("Error loggin in user " + playerName);
                        } else {
                            callback.onLogin(uuid);
                            Log.d(DEBUG, "Logged in: " + result.trim());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error.getLocalizedMessage());
                    }
                });
    }

    @Override
    public void login(String playerName, String password,
                      final RoundRepository.LoginRegisterCallback callback) {
        loginOrRegister(playerName, password, false, callback);
    }

    @Override
    public void register(String playerName, String password, RoundRepository.LoginRegisterCallback callback) {
        loginOrRegister(playerName, password, true, callback);
    }

    private List<Round> roundsFromJSONArray(JSONArray response) {
        List<Round> rounds = new ArrayList<>();
        Round round;
        //String aux;
        for (int i = 0; i < response.length(); i++) {

            try {
                JSONObject o = response.getJSONObject(i);
                String codedboard = o.getString("codedboard");
                Integer id = o.getInt("roundid");
                String playerNames = o.getString("playernames");
                String date = o.getString("dateevent");

                if(codedboard.charAt(0) == '"') {
                    JSONObject json = new JSONObject(codedboard);
                    round = new Round(id.toString(), playerNames.split(",")[0], id.toString(), date, json.toString(), "noestainicializado");
                } else {
                    round = new Round(id.toString(), playerNames.split(",")[0], id.toString(), date, codedboard, "noestainicializado");
                }
                //Round round = new Round(PrefsActivity.getPlayerUUID(context), id.toString());
                //aux = codedboard.substring(1, codedboard.length()-1);

                rounds.add(round);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        return rounds;
    }

    @Override
    public void getRounds(String playeruuid, String orderByField, String group,
                          final RoundsCallback callback) {

        Response.Listener<JSONArray> responseCallback =
                new Response.Listener<JSONArray>() {
                    public void onResponse(JSONArray response) {
                        List<Round> rounds = roundsFromJSONArray(response);
                        Log.d(DEBUG, "Rounds downloaded from server" + rounds.toString());
                        callback.onResponse(rounds);
                    }
                };

        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError("Error downloading rounds from server");
                Log.d(DEBUG, "Error downloading rounds from server");
            }
        };
        is.getOpenRounds(playeruuid, responseCallback, errorCallback);
    }

    public void getActiveRounds(String puuid, final RoundsCallback callback) {
    }

    public void getAllRounds(final String playeruuid, final RoundsCallback callback) {
    }

    @Override
    public void addRound(final Round round, final BooleanCallback callback) {

        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //callback.onError("Error downloading rounds from server");
                callback.onResponse(false);
                Log.d(DEBUG, "Error downloading rounds from server");
            }
        };
        is.newRound(round.getPlayerUUID(),round.getBoard().tableroToString(),new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                String uuid = result.trim();
                Log.d(DEBUG, "round id (addRound): " + uuid);
                callback.onResponse(Integer.parseInt(uuid)>=0);
            }
        }, errorCallback);
    }

    @Override
    public void updateRound(Round round, final BooleanCallback callback) {
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //callback.onError("Error downloading rounds from server");
                callback.onResponse(false);
                Log.d(DEBUG, "Error updating rounds from server");
            }
        };
        is.sendBoard(Integer.parseInt(round.getId()),round.getPlayerUUID(), round.getBoard().tableroToString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String round) {
                Log.d(DEBUG, "round (updateRound): " + round);
                callback.onResponse(true);
            }
        },errorCallback);
    }

    @Override
    public void addPlayerToRound(final Round round, final BooleanCallback callback) {
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //callback.onError("Error downloading rounds from server");
                callback.onResponse(false);
                Log.d(DEBUG, "Error updating rounds from server");
            }
        };
        is.addPlayerToRound(Integer.parseInt(round.getId()),round.getPlayerUUID(), new Response.Listener<String>() {
            @Override
            public void onResponse(String round) {
                Log.d(DEBUG, "round (updateRound): " + round.toString());
                callback.onResponse(true);
            }
        },errorCallback);
    }
}
