package es.uam.eps.dadm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.activities.PrefsActivity;
import es.uam.eps.dadm.view.RecyclerItemClickListener;
import es.uam.eps.dadm.othello.Round;
import es.uam.eps.dadm.database.RoundRepository;
import es.uam.eps.dadm.database.RoundRepositoryFactory;
import es.uam.eps.dadm.view.TableroOthelloView;

/**
 * Fragmento que realiza las operaciones que se ejecutan sobre la actividad de RoundListActivity.
 */
public class RoundListFragment extends Fragment {

    private RecyclerView roundRecyclerView;
    private RoundAdapter roundAdapter;

    private Callbacks callbacks;

    /**
     * Interfaz para gestionar las operaciones que se realizan sobre el Repositorio
     */
    public interface Callbacks {
        /**
         * Callback para la seleccion una partida
         *
         * @param round Partida seleccionada
         */
        void onRoundSelected(Round round);

        /**
         * Callback para la seleccion de la opcion de preferencias
         */
        void onPreferencesSelected();

        /**
         * Callback para la seleccion de la opcion de nueva partida
         */
        void onNewRoundAdded();

        /**
         * Callback para la seleccionon de la opcion de retroceder
         */
        void onGoBackSelected();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Metodo para generar la vista del fragmento
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /* Cargo la vista */
        View view = inflater.inflate(R.layout.fragment_round_list, container, false);

        /* Instancio el recyclerview de partidas */
        roundRecyclerView = (RecyclerView) view.findViewById(R.id.round_recycler_view);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        roundRecyclerView.setLayoutManager(linearLayoutManager);
        roundRecyclerView.setItemAnimator(new DefaultItemAnimator());

        /* Le asigno un listener para cuando se seleccione un elemento de la lista */
        roundRecyclerView.addOnItemTouchListener(new
                RecyclerItemClickListener(getActivity(), new
                RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
                        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
                            @Override
                            public void onResponse(List<Round> rounds) {
                                callbacks.onRoundSelected(rounds.get(position));
                            }

                            @Override
                            public void onError(String error) {
                                Snackbar.make(getView(), R.string.error_reading_rounds,
                                        Snackbar.LENGTH_LONG).show();
                            }
                        };
                        String playeruuid = PrefsActivity.getPlayerUUID(getActivity());
                        repository.getRounds(playeruuid, null, null, roundsCallback);
                    }
                }));

        /* Actualizo la UI */
        updateUI();

        /* Devuelvo la vista */
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    /**
     * Metodo para actualizar la UI del fragmento
     */
    public void updateUI() {
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
            @Override
            public void onResponse(List<Round> rounds) {
                if (roundAdapter == null) {
                    roundAdapter = new RoundAdapter(rounds);
                    roundRecyclerView.setAdapter(roundAdapter);
                } else {
                    roundAdapter = new RoundAdapter(rounds);
                    roundRecyclerView.setAdapter(roundAdapter);
                    roundAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String error) {
            }
        };
        /* Obtengo el UUID del jugador en las preferencias y obtengo sus partidas del repositorio */
        String playeruuid = PrefsActivity.getPlayerUUID(getActivity());
        repository.getRounds(playeruuid, null, null, roundsCallback);
    }

    /**
     * Clase RoundHolder creada para manejar cada elemento de la lista de partidas
     */
    public class RoundHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView idTextView;
        private TableroOthelloView boardView;
        private TextView dateTextView;
        private Round round;

        /**
         * Constrouctor de la clase RoundHolder. Obtiene punteros a cada elemento de la vista de la
         * lista de partidas
         *
         * @param itemView
         */
        public RoundHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            idTextView = (TextView) itemView.findViewById(R.id.list_item_id);
            boardView = (TableroOthelloView) itemView.findViewById(R.id.toview);
            boardView.setClickable(false);
            dateTextView = (TextView) itemView.findViewById(R.id.list_item_date);
        }

        /**
         * Enlaza los datos de la partida con los elementos de la vista
         *
         * @param round
         */
        public void bindRound(Round round) {
            this.round = round;
            idTextView.setText(round.getTitle());
            boardView.setBoard(round.getBoard());
            Boolean he = PrefsActivity.getHelp(getActivity());
            boardView.setHelp(he);
            dateTextView.setText(String.valueOf(round.getDate()).substring(0, 19));
        }

        @Override
        public void onClick(View v) {
            callbacks.onRoundSelected(round);
        }
    }

    /**
     * Clase para manejar el enlace entre la lista de partidas y el manejador de cada elemento
     */
    public class RoundAdapter extends RecyclerView.Adapter<RoundHolder> {
        List<Round> rounds;

        /**
         * Constructor de la clase RoundAdapter
         *
         * @param rounds Lista de partidas existente
         */
        public RoundAdapter(List<Round> rounds) {
            this.rounds = rounds;
        }

        @Override
        public RoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = LayoutInflater.from(parent.getContext());
            View v = li.inflate(R.layout.list_item_view, parent, false);
            return new RoundHolder(v);
        }

        @Override
        public void onBindViewHolder(RoundHolder holder, int position) {
            Round r = rounds.get(position);
            holder.bindRound(r);
        }

        @Override
        public int getItemCount() {
            return rounds.size();
        }
    }

    /**
     * Carga el menu en la pantalla
     *
     * @param menu
     * @param inflater
     */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
    }

    /**
     * Gestiona la opcion del menu que haya sido seleccionada
     *
     * @param item Elemento del menu seleccionado
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /* Opcion de nueva partida: crea una partida, le asigna el UUID y el nombre del jugador
             * y la añade al repositorio */
            case R.id.menu_item_new_round:
                Round round = new Round();
                String playeruuid = PrefsActivity.getPlayerUUID(getActivity());
                round.setPlayerUUID(playeruuid);
                String playername = PrefsActivity.getPlayerName(getActivity());
                round.setFirstPlayerName(playername);

                RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
                RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
                    @Override
                    public void onResponse(boolean response) {
                        callbacks.onNewRoundAdded();
                    }
                };
                repository.addRound(round, callback);
                updateUI();
                return true;
            /* Llama al callback para gestionar la opcion de preferencias */
            case R.id.menu_item_settings:
                callbacks.onPreferencesSelected();
                return true;
            /* Llama al callback para gestionar la opcion de retroceder */
            case R.id.menu_item_refresh:
                updateUI();
                return true;
            default:
                callbacks.onGoBackSelected();
                return true;
        }
    }
}