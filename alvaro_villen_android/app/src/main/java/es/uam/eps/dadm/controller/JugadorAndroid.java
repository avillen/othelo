package es.uam.eps.dadm.controller;

/**
 * Created by alvarovillen on 17/2/17.
 */

import es.uam.eps.dadm.othello.MovimientoOthelo;
import es.uam.eps.dadm.view.TableroOthelloView;
import es.uam.eps.multij.Accion;
import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

/**
 * Clase que implementa la interfaz de Jugador permitiendo usar la terminal para
 * capturar una fila y una columna
 *
 * @author Alvaro Villen
 */
public class JugadorAndroid implements TableroOthelloView.OnPlayListener, Jugador {

    private String nombre;
    private Partida partida;
    private TableroOthelloView tov;

    /**
     * Creates a new instance of JugadorConsola
     */
    public JugadorAndroid(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Recibe una notificacion de un cambio en la partida
     */
    @Override
    public void onCambioEnPartida(Evento evento) {
        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                System.out.println(evento.getDescripcion());
                System.out.println(evento.getPartida().getTablero().getTurno() + " " + evento.getPartida().getTablero().movimientosValidos());
                break;
            case Evento.EVENTO_TURNO:
                tov.invalidate();
                partida = evento.getPartida();
                break;
            case Evento.EVENTO_FIN:
                tov.invalidate();
                System.out.println(evento.getDescripcion());
                break;
            case Evento.EVENTO_ERROR:
                break;
        }
    }

    @Override
    public String getNombre() {
        return this.nombre;
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onPlay(int row, int column) {
        try {
            if (partida.getTablero().getEstado() != Tablero.EN_CURSO) {
                return;
            }
            Movimiento m = new MovimientoOthelo(row, column);
            Accion ac = new AccionMover(this, m);
            try {
                partida.realizaAccion(ac);
            } catch (ExcepcionJuego e) {
                e.printStackTrace();
            }
        } catch (Exception e) {

        }
    }

    public void setTov(TableroOthelloView tov) {
        this.tov = tov;
    }
}
