package es.uam.eps.dadm.database;

import java.util.List;

import es.uam.eps.dadm.othello.Round;


/**
 * Interfaz de la clase RoundRepository. Permite comenicarse con los repositorios
 */
public interface RoundRepository {
    /**
     * Abre el Repositorio
     *
     * @throws Exception
     */
    void open() throws Exception;

    /**
     * Cierra el Repositorio
     */
    void close();

    /**
     * Interfaz de Callback de el Login/Register
     */
    interface LoginRegisterCallback {
        /**
         * Callback para cuando se lleva a cabo un Login
         *
         * @param playerUuid UUID del jugador
         */
        void onLogin(String playerUuid);

        /**
         * Callback para cuando se produce un error
         *
         * @param error Mensaje de ellor
         */
        void onError(String error);
    }

    /**
     * Realiza la operacion de Login sobre el repositorio
     *
     * @param playername Nombre del jugador
     * @param password   Contraseña del jugador
     * @param callback   Operacion de Callback
     */
    void login(String playername, String password, LoginRegisterCallback callback);

    /**
     * Realiza la operacion de Register sobre el repositorio
     *
     * @param playername Nombre del jugador
     * @param password   Contraseña del jugador
     * @param callback   Operacion de Callback
     */
    void register(String playername, String password, LoginRegisterCallback callback);

    /**
     * Interfaz para el Callback de una operacion booleana
     */
    interface BooleanCallback {
        void onResponse(boolean ok);
    }

    /**
     * Obtiene las partidas de un jugador a partir de su UUID
     *
     * @param playeruuid   UUID del jugador sobre el que se quiere realizar la consulta
     * @param orderByField Condicion para ordenar los campos
     * @param group        Condicion de agrupacion de os campos
     * @param callback     Operacion de Callback
     */
    void getRounds(String playeruuid, String orderByField, String group,
                   RoundsCallback callback);

    /**
     * Añade una partida al Repositorio
     *
     * @param round    Partida que se va a añadir
     * @param callback Operacion de Callback
     */
    void addRound(Round round, BooleanCallback callback);

    /**
     * Actualiza una partida
     *
     * @param round    Partida que se va a actualizar
     * @param callback Operacion de Callback
     */
    void updateRound(Round round, BooleanCallback callback);

    public void addPlayerToRound(final Round round, final BooleanCallback callback);

    /**
     * Interfaz de callback para las partidas
     */
    interface RoundsCallback {
        /**
         * Respuesta del callback
         *
         * @param rounds Lista de partidas
         */
        void onResponse(List<Round> rounds);

        /**
         * Respuesta del callback en caso de error
         *
         * @param error Mensaje de error
         */
        void onError(String error);
    }
}