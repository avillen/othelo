package es.uam.eps.dadm.othello;

import es.uam.eps.multij.Movimiento;

/**
 * Clase que extiende de Movimiento para especificar los movimientos del juego
 * Othelo
 *
 * @author Alvaro Villen
 */
public class MovimientoOthelo extends Movimiento {

    private Integer fila;
    private Integer columna;

    /**
     * Inicializa un movimiento en funcion de la fila y la columna que se reciba
     * por argumento
     *
     * @param fila    Posicion de la fila a la que se desea realizar el movimiento
     * @param columna Posicion de la columna a la que se desea realizar el
     *                movimiento
     */
    public MovimientoOthelo(Integer fila, Integer columna) {
        this.fila = fila;
        this.columna = columna;
    }

    @Override
    public String toString() {
        return "C" + this.columna + "-F" + this.fila;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (o == this)
            return true;
        if (!(o instanceof MovimientoOthelo))
            return false;
        MovimientoOthelo mov = (MovimientoOthelo) o;

        if (this.fila == mov.fila && this.columna == mov.columna)
            return true;
        return false;
    }

    /**
     * Getter de fila
     *
     * @return fila
     */
    public Integer getFila() {
        return fila;
    }

    /**
     * Getter de columna
     *
     * @return columna
     */
    public Integer getColumna() {
        return columna;
    }
}