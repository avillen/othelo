package es.uam.eps.dadm.database;

import android.content.Context;

import es.uam.eps.dadm.activities.PrefsActivity;
import es.uam.eps.dadm.server.ServerRepository;

/**
 * Clase encargada de crear repositorios
 */
public class RoundRepositoryFactory {
    /* Variable que determina si el repositorio es local o remoto */
    private static final boolean LOCAL = true;

    /**
     * Crea un repositorio en funcion de la variable estatica para determinar si
     * el servidor se va a encontrar en local o en remoto
     *
     * @param context
     * @return Repositorio creado
     */
    public static RoundRepository createRepository(Context context) {
        RoundRepository repository;
        boolean online = !PrefsActivity.isOnline(context);

        repository = online ? new DataBase(context) :
                ServerRepository.getInstance(context);
        try {
            repository.open();
        } catch (Exception e) {
            return null;
        }
        return repository;
    }
}