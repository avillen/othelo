package es.uam.eps.dadm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.fragments.AlertDialogFragment;
import es.uam.eps.dadm.fragments.RoundFragment;
import es.uam.eps.dadm.fragments.RoundListFragment;
import es.uam.eps.dadm.othello.Round;
import es.uam.eps.multij.Tablero;

/**
 * Actividad donde se muestran el listado de partidas y las distintas opciones. Tambien se implentan
 * los callback de RoundListFragment y RoundFragment
 */
public class RoundListActivity extends AppCompatActivity
        implements RoundListFragment.Callbacks, RoundFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Establece la vista activity_masterdetail (adptado a distintas posiciones) */
        setContentView(R.layout.activity_masterdetail);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new RoundListFragment();
            fm.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
    }

    @Override
    public void onRoundSelected(Round round) {
        if (round.getBoard().getEstado() == Tablero.FINALIZADA) {
            new AlertDialogFragment().show(getSupportFragmentManager(),
                    "ALERT DIALOG");
        } else {
            if (findViewById(R.id.detail_fragment_container) == null) {
                Intent intent = RoundActivity.newIntent(this, round);
                startActivity(intent);
            } else {
                RoundFragment roundFragment = RoundFragment.newInstance(round);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.detail_fragment_container, roundFragment)
                        .commit();
            }
        }
    }

    @Override
    public void onPreferencesSelected() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNewRoundAdded() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        RoundListFragment roundListFragment = (RoundListFragment)
                fragmentManager.findFragmentById(R.id.fragment_container);
        roundListFragment.updateUI();
    }

    @Override
    public void onGoBackSelected() {
        PrefsActivity.delPrefs(RoundListActivity.this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onRoundUpdated(Round round) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        RoundListFragment roundListFragment = (RoundListFragment)
                fragmentManager.findFragmentById(R.id.fragment_container);
        if(roundListFragment!=null)
            roundListFragment.updateUI();
    }

    @Override
    public void onAddPlayer(Round round) {
        return;
    }
}