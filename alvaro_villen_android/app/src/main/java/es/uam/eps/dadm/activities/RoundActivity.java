package es.uam.eps.dadm.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.fragments.RoundFragment;
import es.uam.eps.dadm.othello.Round;

/**
 * Actividad desde donde se ejecuta la partida. Implementa los callbacks de RoundFragment
 * aunque no es necesario ningun tratamiento especial ya que esto se realiza en la actividad
 * de RoundListActivity.
 */
public class RoundActivity extends AppCompatActivity implements RoundFragment.Callbacks {

    /* Claves para comunicar valores con el fragmento */
    public static final String EXTRA_ROUND_ID = "es.uam.eps.dadm.round_id";
    public static final String ARG_FIRST_PLAYER_NAME = "es.uam.eps.dadm.first_player_name";
    public static final String ARG_ROUND_TITLE = "es.uam.eps.dadm.round_title";
    private static final String ARG_ROUND_DATE = "es.uam.eps.dadm.round_date";
    private static final String ARG_PLAYER_ID = "es.uam.eps.dadm.player_id";
    private static final String TABLERO_KEY = "Tablero";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        /* Crea un fragmentManager para acceder al contenedor de la actividad */
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        /* Comprueba si el fragmento existe, y si no es asi inicializa el fragmento con los datos
         * que se obtienen del Intent */
        if (fragment == null) {
            String roundId = getIntent().getStringExtra(EXTRA_ROUND_ID);
            String firstPlayerName = getIntent().getStringExtra(ARG_FIRST_PLAYER_NAME);
            String roundTitle = getIntent().getStringExtra(ARG_ROUND_TITLE);
            String roundDate = getIntent().getStringExtra(ARG_ROUND_DATE);
            String playerId = getIntent().getStringExtra(ARG_PLAYER_ID);
            String roundBoard = getIntent().getStringExtra(TABLERO_KEY);
            RoundFragment roundFragment = RoundFragment.newInstance(new Round(roundId, firstPlayerName, roundTitle, roundDate, roundBoard, playerId));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, roundFragment)
                    .commit();
        }
    }

    /**
     * Crea un nuevo intent donde introduce los valores de la partida pasada por argumento
     *
     * @param packageContext
     * @param round          Partida con los datos a introducir en el Intent
     * @return intent generado
     */
    public static Intent newIntent(Context packageContext, Round round) {
        Intent intent = new Intent(packageContext, RoundActivity.class);
        intent.putExtra(EXTRA_ROUND_ID, round.getId());
        intent.putExtra(ARG_FIRST_PLAYER_NAME, round.getFirstPlayerName());
        intent.putExtra(ARG_ROUND_TITLE, round.getTitle());
        intent.putExtra(ARG_ROUND_DATE, round.getDate());
        intent.putExtra(TABLERO_KEY, round.getBoard().tableroToString());
        intent.putExtra(ARG_PLAYER_ID, round.getPlayerUUID());
        return intent;
    }

    @Override
    public void onRoundUpdated(Round round) {
    }

    @Override
    public void onAddPlayer(Round round) {
    }
}