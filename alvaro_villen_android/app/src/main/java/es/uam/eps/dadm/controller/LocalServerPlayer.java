package es.uam.eps.dadm.controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import es.uam.eps.dadm.activities.PrefsActivity;
import es.uam.eps.dadm.othello.MovimientoOthelo;
import es.uam.eps.dadm.server.ServerInterface;
import es.uam.eps.dadm.view.TableroOthelloView;
import es.uam.eps.multij.Accion;
import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

/**
 * Created by alvarovillen on 23/4/17.
 */

public class LocalServerPlayer implements Jugador, TableroOthelloView.OnPlayListener {
    private static final String DEBUG = "DEBUG";
    private Partida game;
    private Context context;
    private String roundId;
    private TableroOthelloView tov;
    public LocalServerPlayer jug;

    public LocalServerPlayer(Context context, String roundId) {
        this.context = context;
        this.roundId = roundId;
        this.jug = this;
    }

    private boolean isBoardUpToDate(String codedboard) {
        return game.getTablero().tableroToString().equals(codedboard);
    }

    public void onPlay(final int row, final int column) {
        ServerInterface is = ServerInterface.getServer(context);
        Response.Listener<JSONObject> responseListener = new
                Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String turno = response.getString("turn");
                            Log.d(DEBUG, "Turno localserverplayer "+turno);
                            String codedboard = response.getString("codedboard");
                            Log.d(DEBUG, "Codeboard localserverplayer "+codedboard);
                            // Si el turno es del jugador y el tablero está actualizado realiza movimiento
                            if (Integer.parseInt(turno) == 0 && isBoardUpToDate(codedboard)) {
                                Movimiento m = new MovimientoOthelo(row, column);
                                Accion ac = new AccionMover(jug, m);
                                try {
                                    game.realizaAccion(ac);
                                } catch (ExcepcionJuego e) {
                                    e.printStackTrace();
                                }
                                Log.d(DEBUG, "Realizado tablero");
                            }
                            // Si el turno es del jugador pero el tablero no está actualizado actualizar tablero
                            else if (Integer.parseInt(turno) == 0 && !isBoardUpToDate(codedboard)) {
                                game.getTablero().stringToTablero(codedboard);
                                Log.d(DEBUG, "Actualizando tablero");
                            }
                            // Si el turno no es del jugador, mostrar mensaje
                            else {
                                Log.d(DEBUG, "Error por localserverplayer");
                            }

                        } catch (Exception e) {
                            Log.d(DEBUG, "onPlay localserverplayer" + e);
                        }
                    }
                };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(DEBUG,"Localserver player " + error.getMessage());
            }
        };

        is.esMiTurno(Integer.parseInt(roundId), PrefsActivity.getPlayerUUID(context), responseListener, errorListener);
    }

    @Override
    public String getNombre() {
        return game.getJugador(game.getTablero().getTurno()).getNombre();
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {
        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                System.out.println(evento.getDescripcion());
                System.out.println(evento.getPartida().getTablero().getTurno() + " " + evento.getPartida().getTablero().movimientosValidos());
                break;
            case Evento.EVENTO_TURNO:
                //tov.invalidate();
                game = evento.getPartida();
                break;
            case Evento.EVENTO_FIN:
                //tov.invalidate();
                System.out.println(evento.getDescripcion());
                break;
            case Evento.EVENTO_ERROR:
                break;
        }
    }

    public void setTov(TableroOthelloView tov) {
        this.tov = tov;
    }
//
}
