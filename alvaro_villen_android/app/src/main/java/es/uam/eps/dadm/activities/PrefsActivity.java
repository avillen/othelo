package es.uam.eps.dadm.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.database.RoundDataBaseSchema;
import es.uam.eps.dadm.fragments.PrefsFragment;

/**
 * Actividad donde se gestionan las preferencias de la aplicacion
 */
public class PrefsActivity extends AppCompatActivity {

    public final static String HELP_KEY = "help";
    public final static Boolean HELP_DEFAULT = false;
    public final static String PLAYER_KEY = "players";
    public final static String PLAYER_DEFAULT = "1";
    public final static String UUID_DEFAULT = "prueba";
    public final static String PLAYERNAME_DEFAULT = "default";

    public final static String ONLINE_KEY = "online";
    public final static Boolean ONLINE_DEFAULT = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        /* Instancionamos un fragmentManager para realizar
        transacciones con el fragmento de preferencias */
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        PrefsFragment fragment = new PrefsFragment();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Metodo para limpiar las preferencias del usuario
     *
     * @param context
     */
    public static void delPrefs(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().clear().commit();
    }

    /**
     * Clase estatica para obtener el valor de la preferencia Help
     *
     * @param context
     * @return true si existe la clave, false en caso contrario
     */
    public static boolean getHelp(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(HELP_KEY, HELP_DEFAULT);
    }

    /**
     * Clase estatica para obtener el valor de la preferencia NumPlayers
     *
     * @param context
     * @return true si existe la clave, false en caso contrario
     */
    public static int getNumPlayers(Context context) {
        String ret = PreferenceManager.getDefaultSharedPreferences(context).getString(PLAYER_KEY, PLAYER_DEFAULT);
        return Integer.parseInt(ret);
    }

    /**
     * Clase estatica para obtener el valor de la preferencia PlayerName
     *
     * @param context
     * @return true si existe la clave, false en caso contrario
     */
    public static String getPlayerName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(RoundDataBaseSchema.UserTable.Cols.PLAYERNAME, PLAYERNAME_DEFAULT);
    }

    /**
     * Clase estatica para obtener el valor de la preferencia PlayerUUID
     *
     * @param context
     * @return true si existe la clave, false en caso contrario
     */
    public static String getPlayerUUID(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(RoundDataBaseSchema.UserTable.Cols.PLAYERUUID, UUID_DEFAULT);
    }

    /**
     * Clase estatica para establecer el valor de la preferencia PlayerUUID
     *
     * @param context
     * @param playerId valor con el que se va a establecer
     */
    public static void setPlayerUUID(Context context, String playerId) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(RoundDataBaseSchema.UserTable.Cols.PLAYERUUID, playerId);
        editor.commit();
    }

    /**
     * Clase estatica para establecer el valor de la preferencia PlayerName
     *
     * @param context
     * @param playername valor con el que se va a establecer
     */
    public static void setPlayerName(Context context, String playername) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(RoundDataBaseSchema.UserTable.Cols.PLAYERNAME, playername);
        editor.commit();
    }

    /**
     * Clase estatica para establecer el valor de la preferencia PlayerPassword
     *
     * @param context
     * @param password valor con el que se va a establecer
     */
    public static void setPlayerPassword(Context context, String password) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(RoundDataBaseSchema.UserTable.Cols.PLAYERPASSWORD, password);
        editor.commit();
    }

    public static Boolean isOnline(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(ONLINE_KEY, ONLINE_DEFAULT);
    }

    public static void setOnline(Context context, Boolean online) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(ONLINE_KEY, online);
        editor.commit();
    }
}