package es.uam.eps.dadm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.activities.PrefsActivity;
import es.uam.eps.dadm.controller.JugadorAndroid;
import es.uam.eps.dadm.controller.LocalServerPlayer;
import es.uam.eps.dadm.controller.RemotePlayer;
import es.uam.eps.dadm.database.RoundRepository;
import es.uam.eps.dadm.database.RoundRepositoryFactory;
import es.uam.eps.dadm.othello.Round;
import es.uam.eps.dadm.othello.TableroOthello;
import es.uam.eps.dadm.view.TableroOthelloView;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.PartidaListener;
import es.uam.eps.multij.Tablero;

/**
 * Fragmento que realiza las operaciones que se ejecutan sobre la actividad de RoundActivity.
 */
public class RoundFragment extends Fragment implements PartidaListener {
    public static final String DEBUG = "DEBUG";

    /* Claves para comunicar valores con la actividad */
    public static final String ARG_ROUND_ID = "es.uam.eps.dadm.round_id";
    public static final String ARG_FIRST_PLAYER_NAME = "es.uam.eps.dadm.first_player_name";
    public static final String ARG_ROUND_TITLE = "es.uam.eps.dadm.round_title";
    private static final String ARG_ROUND_DATE = "es.uam.eps.dadm.round_date";
    private static final String ARG_PLAYER_ID = "es.uam.eps.dadm.player_id";
    private static final String TABLERO_KEY = "Tablero";

    public Partida partida;
    private TableroOthelloView boardView;
    private Round round;
    private Jugador ja2;
    private Callbacks callbacks;

    private String roundId;
    private String firstPlayerName;
    private String roundTitle;
    private String roundDate;
    private String boardString;
    private String playerId;

    /**
     * Interfaz para realizar las operaciones de Callback de RoundFragment
     */
    public interface Callbacks {
        void onRoundUpdated(Round round);
        void onAddPlayer(Round round);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        if (round != null)
            this.updateRound();
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        startRound();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Crea una nueva instancia del fragmento asignando los datos de la partida
     *
     * @param round Partida con la que intanciar el fragmento
     * @return RoundFragment con los argumentos de la partida
     */
    public static RoundFragment newInstance(Round round) {
        Bundle args = new Bundle();
        args.putString(ARG_ROUND_ID, round.getId());
        args.putString(ARG_FIRST_PLAYER_NAME, round.getFirstPlayerName());
        args.putString(ARG_ROUND_TITLE, round.getTitle());

        args.putString(ARG_ROUND_DATE, round.getDate());
        args.putString(ARG_PLAYER_ID, round.getPlayerUUID());
        args.putString(TABLERO_KEY, round.getBoard().tableroToString());

        RoundFragment roundFragment = new RoundFragment();
        roundFragment.setArguments(args);
        return roundFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (round == null) {
        /* Obtiene los datos de la instancia actual y crea una nueva partida */
            if (getArguments().containsKey(ARG_ROUND_ID))
                roundId = getArguments().getString(ARG_ROUND_ID);
            if (getArguments().containsKey(ARG_FIRST_PLAYER_NAME))
                firstPlayerName = getArguments().getString(ARG_FIRST_PLAYER_NAME);
            if (getArguments().containsKey(ARG_ROUND_TITLE))
                roundTitle = getArguments().getString(ARG_ROUND_TITLE);
            if (getArguments().containsKey(ARG_ROUND_DATE))
                roundDate = getArguments().getString(ARG_ROUND_DATE);
            if (getArguments().containsKey(TABLERO_KEY))
                boardString = getArguments().getString(TABLERO_KEY);
            if (getArguments().containsKey(ARG_PLAYER_ID))
                playerId = getArguments().getString(ARG_PLAYER_ID);
            if (savedInstanceState != null && boardString == null)
                boardString = savedInstanceState.getString(TABLERO_KEY);
            round = new Round(roundId, firstPlayerName, roundTitle, roundDate, boardString, playerId);
        }
        this.chargeRound();
        //this.updateRound();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_round, container, false);

        /* Cargo el titulo de la partida */
        TextView roundTitleTextView = (TextView) rootView.findViewById(R.id.round_title);
        roundTitleTextView.setText(round.getTitle());

        return rootView;
    }

    /**
     * Metodo que gestiona los evento que se producen en la partida y las sincroniza con el Repositorio
     *
     * @param evento
     */
    public void onCambioEnPartida(Evento evento) {
        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                try {
                    round.getBoard().stringToTablero(evento.getPartida().getTablero().tableroToString());
                } catch (ExcepcionJuego excepcionJuego) {
                    excepcionJuego.printStackTrace();
                }
                /* Compruebo si la opcion de ayuda esta activada */
                Boolean he = PrefsActivity.getHelp(getActivity());
                boardView.setHelp(he);
                /* Actualizo el estado de la partida */
                boardView.invalidate();
                this.updateRound();
                break;
            case Evento.EVENTO_FIN:
                /* Si la partida ha terminado, se muestra una alerta */
                //round.setTitle(evento.getDescripcion());
                String aux = round.getTitle();

                if (partida.getTablero().getTurno() == 0) {
                    round.setTitle(aux.concat(" - Ganada"));
                } else {
                    round.setTitle(aux.concat(" - Perdida"));
                }

                boardView.invalidate();

                this.updateRound();
                new AlertDialogFragment().show(getActivity().getSupportFragmentManager(),
                        "ALERT DIALOG");
                break;
        }
    }

    /**
     * Crea una nueva partida, inicializa los jugadores, el tablero de la vista y la da comienzo.
     */
    void startRound() {
        /* Inicializo el jugador */
        //JugadorAndroid ja1 = new JugadorAndroid(firstPlayerName);

        LocalServerPlayer ja1 = new LocalServerPlayer(getActivity(), roundId);

        int he = PrefsActivity.getNumPlayers(getActivity());
        if (he > 1)
            ja2 = new JugadorAndroid("Jugador2");
        else {
            //ja2 = new JugadorAleatorio("ramdom");
            ja2 = new RemotePlayer("ramdom", roundId, "playerid");
        }

        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();

        Log.d(DEBUG, round.toString());
        if(PrefsActivity.getPlayerName(this.getActivity()).equals(round.getFirstPlayerName())) {
            Log.d(DEBUG,"JUGADOR CREADOR");
            jugadores.add(ja1);
            jugadores.add(ja2);
        } else {
            /////
            Log.d(DEBUG,"SEGUNDO JUGADOR");
            round.setPlayerUUID(PrefsActivity.getPlayerUUID(this.getActivity()));
            RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
            /* Generamos el callback para llamar a updateRound del repositorio */
            RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
                @Override
                public void onResponse(boolean response) {
                    if (!response)
                        Snackbar.make(getView(), R.string.error_adding_player,
                                Snackbar.LENGTH_LONG).show();
                    else {
                        if (callbacks != null)
                            callbacks.onAddPlayer(round);
                    }
                }
            };

            repository.addPlayerToRound(round, callback);

            jugadores.add(ja2);
            jugadores.add(ja1);
        }

        /* Añado los jugadores a la partido */

        //if (round != null)
          //  this.chargeRound();
        partida = new Partida(round.getBoard(), jugadores);
        /* Cargo la vista del tablero */
        boardView = (TableroOthelloView) this.getView().findViewById(R.id.toview);
        boardView.setBoard((TableroOthello) partida.getTablero());
        boardView.setOnPlayListener(ja1);
        ja1.setTov(boardView);
        /* Añado el fragmento como observador de la partida */
        partida.addObservador(this);
        registerListeners();
        /* Inicio la partida */
        if(round.getBoard().getEstado()==1)
            partida.comenzar();
    }

    /**
     * Metodo para controlor el evento de llamada al boton de reiniciar partida
     */
    private void registerListeners() {
        FloatingActionButton resetButton = (FloatingActionButton) getView().findViewById(R.id.reset_round_fab);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* Si la partida ya ha terminado, no se puede reiniciar */
                if (round.getBoard().getEstado() != Tablero.EN_CURSO) {
                    Snackbar.make(getView(), R.string.round_already_finished,
                            Snackbar.LENGTH_SHORT).show();
                    return;
                }
                /* Creamos una partida auxiliar para inicializar el tablero de la partida */
                Round aux = new Round();
                try {
                    round.getBoard().stringToTablero(aux.getBoard().tableroToString());
                } catch (ExcepcionJuego excepcionJuego) {
                    excepcionJuego.printStackTrace();
                }
                updateRound();
                boardView.invalidate();
                if (callbacks != null)
                    callbacks.onRoundUpdated(round);
                Snackbar.make(getView(), R.string.round_restarted,
                        Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (round != null)
            this.updateRound();
    }

    /**
     * Actualiza la partida en el repositorio
     */
    private void updateRound() {
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        /* Generamos el callback para llamar a updateRound del repositorio */
        RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
            @Override
            public void onResponse(boolean response) {
                if (!response)
                    Snackbar.make(getView(), R.string.error_updating_round,
                            Snackbar.LENGTH_LONG).show();
                else {
                    if (callbacks != null)
                        callbacks.onRoundUpdated(round);
                }
            }
        };
        /* Actualiza el estado de la partida en el repositorio */
        if(repository!=null && partida.getTablero().getNumJugadas()!=0){
            repository.updateRound(round, callback);
        }
    }


  /*  private String getRoundInfo() {
        String JSONinfo;
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
            @Override
            public void onResponse(List<Round> rounds) {
                for (Round r : rounds) {
                    if (round.getId().equals(r.getId()))
                        try {

                            round.getBoard().stringToTablero(r.getBoard().tableroToString());

                    } catch (ExcepcionJuego excepcionJuego) {
                            excepcionJuego.printStackTrace();
                        }
                }
            }

            @Override
            public void onError(String error) {
                Snackbar.make(getView(), R.string.error_reading_rounds,
                        Snackbar.LENGTH_LONG).show();
            }
        };
        String playeruuid = PrefsActivity.getPlayerUUID(getActivity());
        if(repository!=null)
            repository.getRounds(playeruuid, null, null, roundsCallback);
    }*/

    /**
     * Carga una partida en el repositorio
     */
    private void chargeRound() {
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
            @Override
            public void onResponse(List<Round> rounds) {
                for (Round r : rounds) {
                    if (r.getId().equals(round.getId()))
                        try {
                            round.getBoard().stringToTablero(r.getBoard().tableroToString());
                        } catch (ExcepcionJuego excepcionJuego) {
                            excepcionJuego.printStackTrace();
                        }
                }
            }

            @Override
            public void onError(String error) {
                Snackbar.make(getView(), R.string.error_reading_rounds,
                        Snackbar.LENGTH_LONG).show();
            }
        };
        String playeruuid = PrefsActivity.getPlayerUUID(getActivity());
        if(repository!=null)
            repository.getRounds(playeruuid, null, null, roundsCallback);
    }
}