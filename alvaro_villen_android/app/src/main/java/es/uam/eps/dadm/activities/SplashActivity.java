package es.uam.eps.dadm.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import es.uam.eps.dadm.R;

/**
 * Actividad que genera una actividad splash. Lanza un hilo durante un segundo mostrando una
 * pantalla de carga.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Thread timerTread = new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerTread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}