package es.uam.eps.dadm.othello;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

import static es.uam.eps.dadm.fragments.RoundFragment.DEBUG;

/**
 * Clase que extiene de Tablero y especifica la funcionalidad del tablero para
 * el juego del Othelo
 *
 * @author Alvaro Villen
 */
public class TableroOthello extends Tablero {

    private String[][] tablero;
    private int alto = 8;
    private int ancho = 8;
    static final String J1 = "0";
    static final String J2 = "1";
    static final String LIBRE = ".";


    /**
     * Crea un nuevo tablero inicializando el estado de éste, y coloca las
     * fichas iniciales
     */
    public TableroOthello() {
        this.tablero = new String[this.alto][this.ancho];
        for (int i = 0; i < this.alto; i++)
            for (int j = 0; j < this.ancho; j++)
                this.tablero[i][j] = LIBRE;
        this.estado = Tablero.EN_CURSO;
        this.tablero[this.alto / 2][this.ancho / 2 - 1] = J1;
        this.tablero[this.alto / 2][this.ancho / 2] = J2;
        this.tablero[this.alto / 2 - 1][this.ancho / 2 - 1] = J2;
        this.tablero[this.alto / 2 - 1][this.ancho / 2] = J1;
    }

    @Override
    protected void mueve(Movimiento m) throws ExcepcionJuego {
        // Si el movimiento es válido, lo realiza
        if (this.esValido(m)) {
            MovimientoOthelo mov = (MovimientoOthelo) m;

            this.tablero[mov.getColumna()][mov.getFila()] = this.turno + "";
            this.ultimoMovimiento = m;

            // Al realizar el movimiento, comprobamos si podemos comer en todas
            // las direcciones
            for (int dirX = -1; dirX < 2; dirX++)
                for (int dirY = -1; dirY < 2; dirY++)
                    this.comer(mov, dirX, dirY);

            // Comprueba el estado en el que se ha quedado el estado
            this.checkEstado();

            System.out.println(this.toString());
        } else {
            // Si el movimiento no es valido, comprueba si tiene que terminar o
            // pasar de turno
            if (this.ultimoMovimiento instanceof MovimientoPasar &&
                    this.movimientosValidos().get(0) instanceof MovimientoPasar) {
                if (this.puntuacion()[0] == this.puntuacion()[1])
                    this.estado = Tablero.TABLAS;
                else {
                    // Asigno el turno al ganador
                    if (this.puntuacion()[0] > this.puntuacion()[1])
                        this.turno = 0;
                    else if (this.puntuacion()[0] < this.puntuacion()[1])
                        this.turno = 1;
                    this.estado = Tablero.FINALIZADA;
                }
            } else if (this.movimientosValidos().get(0) instanceof MovimientoPasar &&
                    !(this.ultimoMovimiento instanceof MovimientoPasar)) {
                this.cambiaTurno();
                this.ultimoMovimiento = new MovimientoPasar();
            } else {
                this.ultimoMovimiento = m;
                //this.cambiaTurno();
            }
        }
    }

    /**
     * Metodo para obtener la puntuacion actual de la partida
     *
     * @return En la posicion 0 del array se encuentra la puntuacion de las
     * fichas negras, y en la posicion 1 la de las fichas blancas
     */
    private int[] puntuacion() {
        int puntuacion[] = {0, 0};
        for (int i = 0; i < this.alto; i++) {
            for (int j = 0; j < this.ancho; j++) {
                if (this.tablero[i][j].equals(J1))
                    puntuacion[0]++;
                else if (this.tablero[i][j].equals(J2))
                    puntuacion[1]++;
            }
        }
        return puntuacion;
    }

    /**
     * Comprueba el estado del tablero
     */
    private void checkEstado() {
        Boolean lleno = true;

        // Compruebo si el tablero esta lleno
        for (int i = 0; i < this.alto; i++)
            for (int j = 0; j < this.ancho; j++)
                if (this.tablero[i][j].equals(LIBRE))
                    lleno = false;

        // Si el tablero esta lleno, compruebo quien ha ganado
        if (lleno == true) {
            if (this.puntuacion()[0] == this.puntuacion()[1])
                this.estado = Tablero.TABLAS;
            else {
                if (this.puntuacion()[0] > this.puntuacion()[1])
                    this.turno = 0;
                else if (this.puntuacion()[0] < this.puntuacion()[1])
                    this.turno = 1;
                this.estado = Tablero.FINALIZADA;
            }
        } else // Si no esta lleno, cambio de turno
            this.cambiaTurno();
    }

    /**
     * Metodo que cambia de color las fichas correspondientes en la direccion
     * que se le indique con dirX y dirY
     *
     * @param mov  Movimiente que se acaba de realizar
     * @param dirX Indica la direccion en el eje x en la que se va a comer (-1
     *             izquierda, 1 derecha)
     * @param dirY Indica la direccion en el eje y en la que se va a comer (-1
     *             arriba, 1 abajo) -1 arriba, 1 abajo
     */
    private void comer(MovimientoOthelo mov, int dirX, int dirY) {
        int posX = 0, posY = 0, auxX = 0, auxY = 0;
        String turno_contrario;

        // Obtenemos el turno del contrario
        if (this.turno == 0)
            turno_contrario = J2;
        else
            turno_contrario = J1;

        // Inicializamos los indices
        posX = auxX = mov.getColumna() + dirX;
        posY = auxY = mov.getFila() + dirY;

        // Busca una ficha de su mismo color para "comer" a las que encierra
        while (posX >= 0 && posY >= 0 && posX < this.ancho && posY < this.alto) {
            if (this.tablero[posX][posY].equals(this.turno + "")) {
                // Una vez encontrada la ficha del mismo turno, come a las demas
                while (auxX >= 0 && auxY >= 0) {
                    if (this.tablero[auxX][auxY].equals(turno_contrario))
                        this.tablero[auxX][auxY] = this.turno + "";
                    else
                        break;
                    // Ajustamos los indices
                    auxX += dirX;
                    auxY += dirY;
                }
                break;
            }
            // Si la posicion esta libre, no tiene que comer mas
            if (this.tablero[posX][posY].equals(LIBRE))
                break;
            // Ajustamos los indices
            posX += dirX;
            posY += dirY;
        }
    }

    @Override
    public boolean esValido(Movimiento m) {
        // MovimientoPasar no es valido para mover
        if (this.movimientosValidos().get(0) instanceof MovimientoPasar)
            return false;

        MovimientoOthelo mov = (MovimientoOthelo) m;

        // Comprueba el rango del tablero
        if (mov.getFila() >= this.alto || mov.getColumna() >= this.ancho || mov.getColumna() < 0 || mov.getFila() < 0)
            return false;

        // Compruebo si el movimiento esta en la lista de validos
        if (this.movimientosValidos().contains(mov))
            return true;

        // Cualquier otro caso
        return false;
    }

    @Override
    public ArrayList<Movimiento> movimientosValidos() {
        ArrayList<Movimiento> moves = new ArrayList<>();

        for (int i = 0; i < this.alto; i++)
            for (int j = 0; j < this.ancho; j++) {
                // Busca las fichas del jugador al que le toca
                if (this.tablero[i][j].equals(this.turno + "")) {
                    // Busca los movimientos posibles de esas fichas en todas
                    // las direcciones
                    for (int x = -1; x < 2; x++)
                        for (int y = -1; y < 2; y++)
                            for (Movimiento mov : this.getMovimientosFicha(i, j, x, y))
                                if (!moves.contains(mov))
                                    moves.add(mov);
                }
            }

        // Si no hay movimientos disponibles, introducimos un movimientoPasar
        if (moves.isEmpty())
            moves.add(new MovimientoPasar());

        return moves;
    }

    /**
     * Obtienes una lista de los movimientos posibles que tiene una ficha en una
     * posicion y funcion de la direccion que se tome
     *
     * @param x    Coordenada X de la ficha a comprobar
     * @param y    Coordenada Y de la ficha a comprobar
     * @param dirX Indica la direccion en el eje x en la que se va a comprobar
     *             (-1 izquierda, 1 derecha)
     * @param dirY Indica la direccion en el eje y en la que se va a comprobar
     *             (-1 arriba, 1 abajo)
     * @return ArrayList con los Movimiento posibles para esa ficha
     */
    private ArrayList<Movimiento> getMovimientosFicha(int x, int y, int dirX, int dirY) {
        int posX = 0, posY = 0;
        Boolean flag = false;
        ArrayList<Movimiento> moves = new ArrayList<>();
        String turno_contrario;

        // Obtenemos el turno del contrario
        if (this.turno == 0)
            turno_contrario = J2;
        else
            turno_contrario = J1;

        // Inicializamos los indices
        posX = x + dirX;
        posY = y + dirY;

        // Recorremos el table buscando posiciones validas
        while (posX >= 0 && posY >= 0 && posX < this.ancho && posY < this.alto) {
            // Si es tu ficha no va a haber mas movimientos validos en esa
            // direccion
            if (this.tablero[posX][posY].equals(this.turno + ""))
                break;
            // No es tu ficha, por lo que si la siguiente posicion esta libre,
            // va a ser un moviento valido
            if (this.tablero[posX][posY].equals(turno_contrario))
                flag = true;
            // Esta libre
            if (this.tablero[posX][posY].equals(LIBRE)) {
                if (flag == true) { // Puede poner
                    Movimiento m = new MovimientoOthelo(posY, posX);
                    moves.add(m);
                    break;
                } else
                    break;
            }
            // Ajustamos los indices
            posX += dirX;
            posY += dirY;
        }
        return moves;
    }

    @Override
    public String tableroToString() {
        String res = "";
        JSONObject jo = new JSONObject();
        JSONObject jult = new JSONObject();
        JSONArray array = new JSONArray();

        MovimientoOthelo mov = (MovimientoOthelo) this.ultimoMovimiento;
        // Generamos el fichero JSON
        try {
            jo.put("turno", this.turno);
            jo.put("estado", this.estado);
            jo.put("numJugadas", this.numJugadas);
            if (mov == null) {
                jult.put("fila", -1);
                jult.put("columna", -1);
            } else {
                jult.put("fila", mov.getFila());
                jult.put("columna", mov.getColumna());
            }
            array.put(jult);
            jo.put("ultimoMovimiento", array);
            for (int i = 0; i < this.alto; i++)
                for (int j = 0; j < this.ancho; j++)
                    res = res.concat(this.tablero[i][j]);
            jo.put("tablero", res);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Devolvemos el JSON como String
        return jo.toString();
    }

    @Override
    public void stringToTablero(String cadena) throws ExcepcionJuego {
        int index = 0;
        String tablAux = null;
        try {
            // Carga la cadena como un JSON y carga los datos del tablero
            Log.d(DEBUG, cadena);
            JSONObject obj = new JSONObject(cadena);
            this.turno = obj.getInt("turno");
            this.estado = obj.getInt("estado");
            this.numJugadas = obj.getInt("numJugadas");
            if (((JSONObject) obj.getJSONArray("ultimoMovimiento").get(0)).getInt("fila") == -1
                    || ((JSONObject) obj.getJSONArray("ultimoMovimiento").get(0)).getInt("fila") == -1) {
                this.ultimoMovimiento = null;
            } else {
                this.ultimoMovimiento = new MovimientoOthelo(((JSONObject) obj.getJSONArray("ultimoMovimiento").get(0)).getInt("fila"),
                        ((JSONObject) obj.getJSONArray("ultimoMovimiento").get(0)).getInt("columna"));
            }
            tablAux = new String(obj.getString("tablero"));
        } catch (JSONException e) {
            throw new ExcepcionJuego(e.getMessage());
        }

        if (tablAux.length() != this.alto * this.ancho)
            throw new ExcepcionJuego("La longitud del tablero no es adecuada (64 caracteres)");

        for (int i = 0; i < this.alto; i++) {
            for (int j = 0; j < this.ancho; j++) {
                this.tablero[i][j] = tablAux.charAt(index) + "";
                index++;
                if (!this.tablero[i][j].equals(LIBRE) && !this.tablero[i][j].equals(J1)
                        && !this.tablero[i][j].equals(J2))
                    throw new ExcepcionJuego("Ha introducido caracteres no permitidos (0 negras, 1 blancas, . libre)");
            }
        }
    }

    public String getTablero(int i, int j) {
        return tablero[i][j];
    }

    @Override
    public String toString() {
        String res = "";
        for (int j = 0; j < this.alto; j++) {
            for (int i = 0; i < this.ancho; i++)
                res = res.concat(this.tablero[i][j]);
            res = res.concat("\n");
        }
        return res;
    }

    public int getAlto() {
        return this.alto;
    }

    public int getAncho() {
        return this.ancho;
    }
}