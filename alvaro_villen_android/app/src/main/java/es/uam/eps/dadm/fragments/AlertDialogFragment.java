package es.uam.eps.dadm.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.activities.PrefsActivity;
import es.uam.eps.dadm.activities.RoundActivity;
import es.uam.eps.dadm.activities.RoundListActivity;
import es.uam.eps.dadm.othello.Round;
import es.uam.eps.dadm.database.RoundRepository;
import es.uam.eps.dadm.database.RoundRepositoryFactory;

/**
 * Fragmento que muestra un mensaje de alerta para terminar o iniciar una nueva partida.
 */
public class AlertDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.game_over);
        alertDialogBuilder.setMessage(R.string.game_over_message);
        alertDialogBuilder.setPositiveButton("Yes",
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    /* Creamos una nueva partida y la introducimos en el repositorio */
                    Round round = new Round();
                    String playeruuid = PrefsActivity.getPlayerUUID(getActivity());
                    round.setPlayerUUID(playeruuid);
                    String playername = PrefsActivity.getPlayerName(getActivity());
                    round.setFirstPlayerName(playername);

                    RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
                    RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
                        @Override
                        public void onResponse(boolean response) {
                        }
                    };
                    repository.addRound(round, callback);

                    if (activity instanceof RoundListActivity)
                        ((RoundListActivity) activity).onRoundUpdated(round);
                    else
                        ((RoundActivity) activity).finish();
                    dialog.dismiss();
                }
            });
        alertDialogBuilder.setNegativeButton("No",
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //if (activity instanceof RoundActivity)
                        //activity.finish();
                    dialog.dismiss();
                }
            });
        return alertDialogBuilder.create();
    }
}