package es.uam.eps.dadm.controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import es.uam.eps.dadm.server.ServerInterface;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Tablero;

import static es.uam.eps.dadm.fragments.RoundFragment.DEBUG;

/**
 * Created by alvarovillen on 23/4/17.
 */

public class RemotePlayer implements Jugador {
    private String nombre;
    private Context context;
    private String roundId;
    private String playerId;

    public RemotePlayer(String nombre, String roundId, String playerId) {
        this.nombre = nombre;
        this.roundId = roundId;
        this.playerId = playerId;
    }

    @Override
    public String getNombre() {
        return this.nombre;
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {
        if (evento.getTipo() == Evento.EVENTO_TURNO) {

            ServerInterface is = ServerInterface.getServer(context);

            Response.Listener<String> responseListener = new
                    Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(DEBUG, "Remote player: " + response);
                        }
                    };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            };

            //String json = evento.getPartida().getTablero().tableroToString();
            //try {
            //  JSONObject o = new JSONObject(json);
            //  Integer roundid = o.getInt("roundid");
            if (evento.getPartida().getTablero().getNumJugadas() > 0)
                is.sendBoard(Integer.parseInt(roundId), playerId, evento.getPartida().getTablero().tableroToString(),
                        responseListener, errorListener);
            //} catch (JSONException e) {
            //  e.printStackTrace();
            //}
        }
    }
}
