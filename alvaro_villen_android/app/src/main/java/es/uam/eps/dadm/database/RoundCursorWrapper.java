package es.uam.eps.dadm.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import es.uam.eps.dadm.othello.Round;
import es.uam.eps.multij.ExcepcionJuego;

/**
 * Created by alvarovillen on 19/3/17.
 */
public class RoundCursorWrapper extends CursorWrapper {
    private final String DEBUG = "DEBUG";

    public RoundCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Round getRound() {
        String playername = getString(getColumnIndex(RoundDataBaseSchema.UserTable.Cols.PLAYERNAME));
        String roundDate = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.DATE));
        String roundTitle = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.TITLE));
        String board = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.BOARD));
        String rounduuid = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.ROUNDUUID));
        String uuid = getString(getColumnIndex(RoundDataBaseSchema.UserTable.Cols.PLAYERUUID));

        Round round = new Round();
        round.setFirstPlayerName("random");
        round.setSecondPlayerName(playername);
        round.setPlayerUUID(uuid);
        round.setDate(roundDate);
        round.setTitle(roundTitle);
        round.setId(rounduuid);

        try {
            round.getBoard().stringToTablero(board);
        } catch (ExcepcionJuego e) {
            Log.d(DEBUG, "Error turning string into tablero");
        }
        return round;
    }
}
