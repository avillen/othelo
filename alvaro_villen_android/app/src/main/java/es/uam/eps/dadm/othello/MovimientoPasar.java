package es.uam.eps.dadm.othello;

import es.uam.eps.multij.Movimiento;

/**
 * Clase que exitiende de movimiento. Nos permite crear un movimiento auxiliar
 * para indicar que el jugador no tiene movimientos disponibles y quiere pasar
 * de turno.
 *
 * @author Alvaro Villen
 */
public class MovimientoPasar extends Movimiento {
    @Override
    public String toString() {
        return "Pasar";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (o == this)
            return true;
        if (!(o instanceof MovimientoOthelo))
            return false;
        return false;
    }
}