package es.uam.eps.dadm.othello;

import java.util.Date;
import java.util.UUID;

import es.uam.eps.multij.ExcepcionJuego;

/**
 * Clase para gestionar las partidas
 */
public class Round {
    private String id;
    private String title;
    private String date;
    private TableroOthello board;
    private String firstPlayerName;
    private String secondPlayerName;
    private String playerUUID;

    /**
     * Constructor de la clase Round. Genera un identificador, le asigna un titulo y una fecha
     * e instancia un nuevo tablero
     */
    public Round() {
        id = UUID.randomUUID().toString();
        title = "ROUND " + id.toString().substring(19, 23).toUpperCase();
        date = new Date().toString();
        board = new TableroOthello();
    }

    /**
     * Constructor de la clase Round. Genera un identificador, le asigna un titulo y una fecha
     * e instancia un nuevo tablero
     */
    public Round(String playerUUID, String id) {
        this.id = id;
        this.playerUUID = playerUUID;
        title = "ROUND " + id;
        date = new Date().toString();
        board = new TableroOthello();
    }

    /**
     * Crea una partida inicializada con los datos pasados por argumento
     *
     * @param roundId         UUID de la partida
     * @param firstPlayerName Nombre del jugador
     * @param roundTitle      Titulo de la partida
     * @param roundDate       Fecha de inicio de la partida
     * @param roundBoard      Tablero de la partida en formato JSON
     * @param playerId        UUID del jugador
     */
    public Round(String roundId, String firstPlayerName, String roundTitle, String roundDate, String roundBoard, String playerId) {
        this.id = roundId;
        this.firstPlayerName = firstPlayerName;
        this.title = roundTitle;
        this.date = roundDate;
        this.board = new TableroOthello();
        try {
            this.board.stringToTablero(roundBoard);
        } catch (ExcepcionJuego excepcionJuego) {
            excepcionJuego.printStackTrace();
        }
        this.playerUUID = playerId;
    }

    /* Getters */
    public String getFirstPlayerName() {
        return firstPlayerName;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPlayerUUID() {
        return playerUUID;
    }

    public String getDate() {
        return date;
    }

    public TableroOthello getBoard() {
        return board;
    }

    /* Setters */
    public void setId(String id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFirstPlayerName(String firstPlayerName) {
        this.firstPlayerName = firstPlayerName;
    }

    public void setSecondPlayerName(String secondPlayerName) {
        this.secondPlayerName = secondPlayerName;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    @Override
    public String toString() {
        return this.firstPlayerName + " " + this.secondPlayerName + " " + this.id + " " + this.playerUUID;
    }
}
