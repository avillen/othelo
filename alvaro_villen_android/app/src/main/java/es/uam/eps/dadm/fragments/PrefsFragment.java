package es.uam.eps.dadm.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import es.uam.eps.dadm.R;

/**
 * Fragmento que realiza las operaciones que se ejecutan sobre la actividad de PrefsActivity.
 */
public class PrefsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.addPreferencesFromResource(R.xml.prefes);
    }
}
