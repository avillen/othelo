package es.uam.eps.dadm.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import es.uam.eps.dadm.othello.MovimientoOthelo;
import es.uam.eps.dadm.othello.TableroOthello;
import es.uam.eps.multij.Tablero;


/**
 * Clase que extiende de view e implementa la vista del Tablero Othello
 */
public class TableroOthelloView extends View {

    private Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private float heightOfTile;
    private float widthOfTile;
    private float radio;
    private boolean help;

    private TableroOthello board;

    private OnPlayListener onPlayListener;

    /**
     * Interfaz para controlar los eventos que suceden sobre la vista del tablero
     */
    public interface OnPlayListener {
        void onPlay(int row, int column);
    }

    /**
     * Constructor de la vista del tablero
     *
     * @param context
     */
    public TableroOthelloView(Context context) {
        super(context);
    }

    /**
     * Constructor de la vista del tablero. Establece la ayuda a falso por defecto e inicializa
     * los parametros basicos para dibujar el tablero
     *
     * @param context
     * @param attrs
     */
    public TableroOthelloView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.help = false;
        init();
    }

    /**
     * Constructor de la vista del tablero
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public TableroOthelloView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Establece el color de fondo del tablero y establece el grosor de las lineas
     */
    private void init() {
        backgroundPaint.setColor(Color.DKGRAY);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        if (widthSize < heightSize)
            width = height = heightSize = widthSize;
        else
            width = height = widthSize = heightSize;

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (this.board != null) {
            widthOfTile = w / this.board.getAncho();
            heightOfTile = h / this.board.getAlto();
            if (widthOfTile < heightOfTile)
                radio = widthOfTile * 0.4f;
            else
                radio = heightOfTile * 0.4f;
        }
        super.onSizeChanged(w, h, oldw, oldh);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, getWidth(), getHeight(), backgroundPaint);
        if (board != null)
            drawCircles(canvas, linePaint);
    }

    /**
     * Dibuja los circulos que representaran las fichas de los jugadores
     *
     * @param canvas
     * @param paint
     */
    private void drawCircles(Canvas canvas, Paint paint) {
        float centerRaw, centerColumn;
        for (int i = 0; i < this.board.getAlto(); i++) {
            centerRaw = heightOfTile * (1 + 2 * i) / 2f;
            for (int j = 0; j < this.board.getAncho(); j++) {
                centerColumn = widthOfTile * (1 + 2 * j) / 2f;
                setPaintColor(paint, j, i);
                canvas.drawCircle(centerColumn, centerRaw, radio, paint);
            }
        }
    }

    /**
     * Establece el color de las fichas en funcion de la posicon
     *
     * @param paint
     * @param i     Coordenada i del tablero
     * @param j     Coordenada j del tablero
     */
    private void setPaintColor(Paint paint, int i, int j) {
        if (this.board.movimientosValidos().contains(new MovimientoOthelo(j, i)) && this.help) {
            paint.setColor(Color.YELLOW);
        } else if (board.getTablero(i, j).equals("0"))
            paint.setColor(Color.BLACK);
        else if (board.getTablero(i, j).equals("1"))
            paint.setColor(Color.WHITE);
        else
            paint.setColor(Color.GRAY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (board.getEstado() != Tablero.EN_CURSO)
            return true;
        if (event.getAction() == MotionEvent.ACTION_DOWN && onPlayListener != null) {
            onPlayListener.onPlay(fromEventToI(event), fromEventToJ(event));
        }
        return true;
    }

    /* Setters */
    public void setHelp(boolean help) {
        this.help = help;
    }

    public void setOnPlayListener(OnPlayListener listener) {
        this.onPlayListener = listener;
    }

    public void setBoard(TableroOthello board) {
        this.board = board;
    }

    /* Traducimos pixeles en fila y columna del tablero */
    private int fromEventToI(MotionEvent event) {
        return (int) (event.getY() / heightOfTile);
    }

    private int fromEventToJ(MotionEvent event) {
        return (int) (event.getX() / widthOfTile);
    }
}