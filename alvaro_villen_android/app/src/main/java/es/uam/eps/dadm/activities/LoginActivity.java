package es.uam.eps.dadm.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.uam.eps.dadm.R;
import es.uam.eps.dadm.database.RoundRepository;
import es.uam.eps.dadm.database.RoundRepositoryFactory;


/**
 * Actividad donde se llevara a cabo la accion del login
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    private RoundRepository repository;
    private EditText usernameEditText;
    private EditText passwordEditText;

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


    /**
     * Metodo onCreate
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        PrefsActivity.setOnline(this, false);
        if (isOnline())
            PrefsActivity.setOnline(this, true);


        /* Comprueba si existe algun nombre en las preferencias */
        if (!PrefsActivity.getPlayerName(this).equals(PrefsActivity.PLAYERNAME_DEFAULT)) {
            startActivity(new Intent(LoginActivity.this, RoundListActivity.class));
            finish();
            return;
        }
        /* Declaramos los elementos de la pantalla y añadimos listeners a los botonoes */
        usernameEditText = (EditText) findViewById(R.id.login_username);
        passwordEditText = (EditText) findViewById(R.id.login_password);
        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);
        TextView newUserButton = (TextView) findViewById(R.id.new_user_button);
        newUserButton.setOnClickListener(this);
        TextView cancelButton = (TextView) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(this);

        /* Creamos un repositorio para acceder a la base de datos*/
        repository = RoundRepositoryFactory.createRepository(LoginActivity.this);
        if (repository == null)
            Toast.makeText(LoginActivity.this, R.string.repository_opening_error,
                    Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        /* Recogemos el cantenido de los campos de login */
        final String playername = usernameEditText.getText().toString();
        final String password = passwordEditText.getText().toString();

        /* Implementamos los callbacks de login/register */
        RoundRepository.LoginRegisterCallback loginRegisterCallback =
                new RoundRepository.LoginRegisterCallback() {
                    @Override
                    public void onLogin(String playerId) {
                        /* Al hacer onLogin, guarda los datos en las preferencias */
                        PrefsActivity.setPlayerUUID(LoginActivity.this, playerId);
                        PrefsActivity.setPlayerName(LoginActivity.this, playername);
                        startActivity(new Intent(LoginActivity.this, RoundListActivity.class));
                        /* Finaliza la actividad */
                        finish();
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(LoginActivity.this, /*R.string.user_no_exist*/error,
                                Toast.LENGTH_SHORT).show();
                    }
                };
        switch (v.getId()) {
            case R.id.login_button:
                /* Realiza la llamada de login sobre el repositorio */
                repository.login(playername, password, loginRegisterCallback);
                break;
            case R.id.cancel_button:
                finish();
                break;
            case R.id.new_user_button:
                /* Realiza la llamada de registro sobre el repositorio */
                repository.register(playername, password, loginRegisterCallback);
                break;
        }
    }
}